<?php
/**
 * Website: https://gitlab.com/ekram/web-scrapper
 * Acknowledge: A. S. M. Ekram Hossain (hhttps://gitlab.com/ekram)
 *
 * Licensed Free
 *
 * Authors:
 *   A. S. M. Ekram Hossain
 *   
 *
 * Version Rev. 1.0 
 */


class dataScrapper{

    private $dom;
    private $xpath;
    private $xpathContents;
    private $xpathquery='';
    private $search=[]; 
    private $url='';
    private $itemTarget='*';
    private $itemContent='*';   
    private $result=[];
    private $internalErrors='';


    function __construct($param=[])
    {
        $this->dom = new DomDocument('1.0', 'UTF-8'); 
        $this->dom->preserveWhiteSpace = false;
        $this->dom->formatOutput = true;
        $this->internalErrors = libxml_use_internal_errors(true);
        array_walk($param, function($item,$index){$this->{$index}=$item;});
        $this->getHtmlContent();
        $this->createQueryPattern();
        $this->getItems();
    }

    /*
    * public function result()
    * This will return the items in array format
    *  No input requred
    */
    function result()
    {
        return $this->result;
    }

    /*
    * public function resultJson()
    * This will return the items in json format
    *  No input requred
    */
    function resultJson()
    {
        return json_encode($this->result);
    }

    /*
    * public function resultObject()
    * This will return the items in Object format
    *  No input requred
    */
    function resultObject()
    {
        return (object)$this->result;
    }


    /*
    * $this->createQueryByTag() 
    * It will receive html tag as parameter
    *  It will not return any output
    * It will set xpathquery pattern by tag for query
    */
    private function createQueryByTag($tag)
    {
        $this->xpathquery="//{$tag}";
    }

    /*
    * $this->createQueryByClass() 
    * It will receive html class name and tag as parameter
    *  It will not return any output
    * It will set xpathquery pattern by tag and classname for query
    */
    private function createQueryByClass($className,$tag='*')
    {
        $this->xpathquery="//{$tag}[contains(@class, '{$className}')]";
    }

    /*
    * $this->createQueryById() 
    * It will receive html id and tag as parameter
    *  It will not return any output
    * It will set xpathquery pattern by tag and id for query
    */
    private function createQueryById($idValue,$tag='*')
    {
        $this->xpathquery="//{$tag}[contains(@id, '{$idValue}')]";
    }

    /*
    * $this->createQueryByAny() 
    * It will receive html attribute  and tag as parameter
    *  It will not return any output
    * It will set xpathquery pattern by tag and attribute for query
    */
    private function createQueryByAny($attribute,$attributeValue,$tag='*')
    {
        $this->xpathquery="//{$tag}[contains(@{$attribute},'{$attributeValue}')]";
    }

    /*
    * $this->createQueryPattern() 
    * This method works to create query pattern
    *  it will check search format and key and the call proper method to create the pattern
    * It will not return any value, will set pattern to property
    */
    private function createQueryPattern()
    {
        if(isset($this->search['searchBy'])){
            $tag=isset($this->search['tag'])?$this->search['tag']:"*";
            if($this->search['searchBy']!='custom' && $this->search['searchBy']!='any'){
                $queryBy='createQueryBy'.ucfirst($this->search['searchBy']);
                $this->{$queryBy}($this->search['searchKey'],$tag);
            }elseif($this->search['searchBy']=='any'){
                $this->createQueryByAny($this->search['attribute'],$this->search['searchKey'],$tag);
            }elseif($this->search['searchBy']=='custom'){
                $this->xpathquery=$this->search['searchKey'];
            }else{
                throw new Exception('Invalid search format!!!');
            }
            $this->xpathquery;
        }
    }


    /*
    * $this->getHtmlContent() 
    * This method will load html contents fro url
    * It will raise exception if can't load html file 
    * If the url is ok, then it will load contant to xpathContents property
    */
    private function getHtmlContent()
    {
        if(!@$this->dom->loadHTMLFile($this->url)){            
            throw new Exception('Failed to load html from url, please check your url');
        }  
        libxml_use_internal_errors($this->internalErrors);           
        $this->xpathContents = new DOMXpath($this->dom);        
    }


    /*
    * $this->getElementAttributes() 
    * This method will receive attribute objects as parameter and 
    * Returns the formatted array of attribute name and values
    */
    private function getElementAttributes($itemAttributes)
    {
        $attributes=[];
        if($itemAttributes!=null){
            foreach($itemAttributes as $attr){                        
                $attributes[$attr->name]=$attr->nodeValue;
            }
        }        
        return $attributes;
    }


    /*
    * $this->getSelectedAttributes() 
    * This method will receive content and attribute objects as parameter and 
    * Returns the formatted array of attribute for user selected attribute name 
    */
    private function getSelectedAttributes($contents,$attributes)
    {
        return $contents->getAttribute($attributes);
    }
    

    /*
    * $this->getElementChilds() 
    * This method will receive contentobjects as parameter and 
    * Returns the formatted array of child nodes 
    */
    private function getElementChilds($contents)
    {
        $childs=[];
        foreach ($contents as $node) {
            if(property_exists($node,'childNode') && $node->childNode->length>0){
                $childs[] = $this->getElementChilds($node->childNode);
            }else{
                $childs[] = [
                    'tagName'=>(property_exists($node,'tagName'))?$node->tagName:$node->nodeName,
                    'contents'=>preg_replace(['(\s+)u', '(^\s|\s$)u'], [' ', ''], $node->textContent) ,
                    'attr'=>$this->getElementAttributes($node->attributes)
                ];
            }            
        }
        return $childs;
    }


    /*
    * $this->getItems() 
    * This method will retrive the items in array format
    * And store it in result property
    */
    private function getItems()
    {
        $elements = $this->xpathContents->query($this->xpathquery);
        if (!is_null($elements)) {	
            $resultarray=array();
            foreach ($elements as $element) {
                $resultarray[]=[
                    'tagName'=>$element->tagName , 
                    'contents'=>preg_replace(['(\s+)u', '(^\s|\s$)u'], [' ', ''], $element->textContent) , 
                    'childs'=>$this->getElementChilds($element->childNodes),
                    'attr'=>$this->getElementAttributes($element->attributes)
                ];               
            }
        }
        $this->result=$resultarray;

    }
}

